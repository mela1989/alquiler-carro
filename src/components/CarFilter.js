import React from "react";
import { useContext, useState } from "react";
import {CarContext} from '../context';
import Title from '../components/Titel';
//import DatePicker from "react-datepicker";
//import "react-datepicker/dist/react-datepicker.css";

    

const getUnique =(items, value)=>{
    return [...new Set(items.map(item => item[value]))]
}

const  CarFilter = ({cars}) => {
   // const [startDate, setStartDate] = useState(new Date("2014/02/08"));
  //  const [endDate, setEndDate] = useState(new Date("2014/02/10"));
    
   const context = useContext(CarContext);
   const {
    handleChange,
    type,
    capacity,
    price,
    minPrice,
    maxPrice,
    date
  } = context;

  let types = getUnique(cars, 'type');
  types = ['all', ...types];
  types = types.map((item, index) => (
    <option key={index} value={item}>
      {item}
    </option>
  ));
  
  let people = getUnique(cars, "capacity");
  people = people.map((item, index) => (
    <option key={index} value={item}>
      {item}
    </option>
  ));
    return (
        <section className="filter-container">
        <Title title="search cars" />
        <form className="filter-form">
          {/* tipo */}
          <div className="form-group">
            <label htmlFor="type">Tipo de carro</label>
            <select
              name="type"
              id="type"
              onChange={handleChange}
              className="form-control"
              value={type}
            >
              {types}
            </select>
          </div>
         
          {/* gente que cabe en el carro */}
          <div className="form-group">
            <label htmlFor="capacity">Capacidad de personas</label>
            <select
              name="capacity"
              id="capacity"
              onChange={handleChange}
              className="form-control"
              value={capacity}
            >
              {people}
            </select>
          </div>
      
          {/* precio */}
          <div className="form-group">
            <label htmlFor="price">Precio del carro ${price}</label>
            <input
              type="range"
              name="price"
              min={minPrice}
              max={maxPrice}
              id="price"
              value={price}
              onChange={handleChange}
              className="form-control"
            />
          </div>
         

          
{/*  <div className="form-group">
            <label htmlFor="date">Fechas</label>
            <DatePicker
        selected={startDate}
        onChange={date => setStartDate(date)}
        selectsStart
        startDate={startDate}
        endDate={endDate}
        name='date'
        id='date'
        placeholderText='Desde'
        className="form-control"
      />
      <DatePicker
        selected={endDate}
        onChange={date => setEndDate(date)}
        selectsEnd
        endDate={endDate}
        minDate={startDate}
        name='date'
        id='date'
        placeholderText='Hasta'
        className="form-control"
      />
    </div> */}
     
     
      </form>
    </section>
    );
};

export default CarFilter;
