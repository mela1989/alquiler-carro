import React, { Component } from 'react';
import mainbg from '../images/mainbg.jpg';
import Premium from '../components/Premium';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';
import {CarContext} from '../context';
import StyledPremium from '../components/styledPremium';

export default class SingleCar extends Component {
    constructor(props) {
        super(props);
        //console.log(this.props); ahi se puede ver de donde viene el slug
        this.state = {
            slug:this.props.match.params.slug,
            mainbg
        };
    }
    static contextType = CarContext;
    //componentDidMount() {}
    render() {
        const { getCar } = this.context;
        const car = getCar(this.state.slug);
        if(!car){
            return (
                <div className='error'>
                <h3>No se encontro carros disponibles en este momento</h3>
                <Link to='/cars' className='btn-primary'>Volver atras</Link>
            </div>
            );
        }
        const {
            name, 
            description, 
            capacity, 
            price, 
            extras, 
            images, 
            type
        } = car
        return (
            <>
           <StyledPremium img={images[0] || this.state.mainbg}>
                <Banner title={`Carro ${name}`}>
                    <Link to='/cars' className='btn-primary'>
                        Volver a otro carros
                    </Link>
                </Banner>
            </StyledPremium>
            <section>
                <div className='single-car-info'>
                  
                    <article className='desc'>
                        <h3>Detalles del carro</h3>
                        <p>{description}</p>
                        </article>
                    <article className='info'>
                        <h3>Informacion sobre el carro</h3>
                        <h6>Precio: ${price}</h6>
                        <h6>Maxima capacidad: {capacity > 1 ? `${capacity} personas` : 
                    `${capacity} personas`} </h6>
                        <h6>Tipo de carro: {type}</h6>
                        <h6>Extras: {extras}</h6>
                    
                    </article>
                </div>
            </section>
            </>
        );
    }
}

