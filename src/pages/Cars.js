import React from 'react'
import Premium from '../components/Premium';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';
import CarContainer from '../components/CarContainer';

const CarPage = () => {
    return(
        <>
        <Premium premium='carsPremium'>
            <Banner title='Nuestros carros'>
                <Link to='/' className='btn-primary'>
                    Volver al Home
                </Link>
            </Banner>

        </Premium>
        <CarContainer />
        
        </>
    );
    
};

export default CarPage;
