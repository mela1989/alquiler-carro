import React from 'react'
import Premium from '../components/Premium';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';
import Services from '../components/Services';
import CarsFeatured from '../components/CarsFeatured';

export default function Home() {
        return (
            <>
            <Premium>
            <Banner 
                title='ALQUILER DE CARROS' 
                subtitle='Alquila un carro en el mejor precio' >
                    <Link to='/cars' className='btn-primary'>
                        Carros disponibles
                    </Link>
            </Banner>            
        </Premium>
        <Services />
        <CarsFeatured />
        </>
        );
    }



